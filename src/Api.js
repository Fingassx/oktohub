const api = 'https://api.github.com';
const userEndPoint = `${api}/user`;
const reposEndPoint = `${api}/user/repos`;
const getEventsEndPoint = (username) => `${api}/users/${username}/received_events`;

export const getUserInfo = async (token) => {
    const headers = {
        "Authorization": `Basic ${token}`
    }
    const response = await fetch(userEndPoint, {
        method: 'GET',
        headers
    })
    if(response.status !== 200){
        return false;
    }

    const user = await response.json();
    return user;
}

export const getUserRepos = async (token) => {
    const headers = {
        "Authorization": `Basic ${token}`
    }
    const response = await fetch(reposEndPoint, {
        method: 'GET',
        headers
    })
    if(response.status !== 200){
        return false;
    }

    const repos = await response.json();
    return repos;
}

export const getUserEvents = async (token, userName) => {
    const headers = {
        "Authorization": `Basic ${token}`
    }
    const response = await fetch(getEventsEndPoint(userName), {
        method: 'GET',
        headers
    })
    if(response.status !== 200){
        return false;
    }

    const events = await response.json();
    return events;
}