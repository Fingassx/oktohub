import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, AsyncStorage} from 'react-native';
import {Provider} from 'react-redux';
import store from './store';
import Route from './Route';

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      signedIn: false,
      checkedSignIn: false
    };
  }

  componentWillMount() {
    this.isSignedIn()
      .then((res) => {
        this.setState({signedIn: res, checkedSignIn: true});
      })
      .catch((err) => alert('An error occurred'));
  }

  isSignedIn = () => {
    return new Promise((resolve, reject) => {
      AsyncStorage.getItem('token')
        .then((res) => {
          if (res !== null) {
            resolve(true);
          } else {
            resolve(false);
          }
        })
        .catch((err) => reject(err));
    });
  };

  render() {
    const {checkedSignIn, signedIn} = this.state;

    // If we haven't checked AsyncStorage yet, don't render anything (better ways to do this)
    if (!checkedSignIn) {
      return null;
    }

    const route = Route(signedIn);
    return (
      <Provider store={store}>
        <View style={styles.container}>{route}</View>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5
  }
});
