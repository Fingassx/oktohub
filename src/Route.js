import React from 'react';
import Icon from 'react-native-vector-icons/Octicons';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import {View} from 'react-native';
import {createBottomTabNavigator, createStackNavigator} from 'react-navigation';

import WelcomeScreen from './screens/WelcomeScreen'
import AnotherScreen from './screens/AnotherScreen'
import Feed from './screens/Feed'
import Projects from './screens/Projects'
import Profile from './screens/Profile'

export default (Route = (isLoggedIn) => {
  const MainNavigator = createStackNavigator(
    {
      intro: {
        screen: WelcomeScreen
      },
      home: createBottomTabNavigator({
        Feed: {
          screen: Feed,
          tabBarIcon: <Icon name="mark-github" size={20} color="black" iconStyle={{flex: 1}}/>
        },
        Projects: {
          screen: Projects,
          tabBarIcon: <MaterialIcon name="folder" iconStyle={{width: '100%', height: '50%'}}/>
        },
        Profile: {
          screen: Profile,
          tabBarIcon: <MaterialIcon name="account-circle" iconStyle={{width: '100%', height: '50%'}}/>
        },
      })
    },
    {
      initialRouteName: isLoggedIn ? 'home' : 'intro',
      animationEnabled: true,
      backBehavior: 'none',
      headerMode: 'none',
      tabBarOptions: {
        showIcon: true
      }
    }
  );

  return <MainNavigator />;
});