import {SET_EVENTS} from './types';

export const setUserEvents = (userData) => (dispatch) => {
    console.log(userData)
    dispatch({
        type: SET_EVENTS,
        payload: userData
    });
}