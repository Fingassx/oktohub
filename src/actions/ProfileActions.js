import {SET_PROFILE_DATA} from './types';

export const setProfileData = (userData) => (dispatch) => {
    console.log(userData)
    dispatch({
        type: SET_PROFILE_DATA,
        payload: userData
    });
}