import {SET_REPOS} from './types';

export const setUserProjects = (userData) => (dispatch) => {
    console.log(userData);
    dispatch({
        type: SET_REPOS,
        payload: userData
    });
}