export const SET_PROFILE_DATA = "SET_PROFILE_DATA";

export const SET_REPOS = "SET_REPOS";

export const SET_EVENTS = "SET_EVENTS";