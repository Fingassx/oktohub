import React from 'react';
import {StyleSheet, Image, View, Text, AsyncStorage, ActivityIndicator, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Octicons';
import TimeAgo from 'react-native-timeago';

const EventItem = ({actor, payload, repo, isFirst, created_at}) => {
  return (
    <View style={[styles.outerContainer, isFirst ? {marginTop: 96} : {}]}>
      <TouchableOpacity style={styles.innerContainer}>
        <Image style={styles.avatar} source={{uri: actor.avatar_url}} />
        <View style={styles.outerRow}>
          <View style={styles.row}>
            <Text>
              <Text style={{fontWeight: '800'}}>{actor.login}</Text>
              {' added comment to issue '}
              <Text style={{fontWeight: '800'}}>
                {'#'}
                {payload.issue.number}
              </Text>
              {' at '}
              {repo.name}
            </Text>
          </View>
          <View style={styles.row}>
            <TimeAgo time={created_at} />
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  outerContainer: {
    padding: 8,
    paddingLeft: 16,
    paddingRight: 16,
    elevation: 0
  },
  innerContainer: {
    borderRadius: 5,
    elevation: 2,
    backgroundColor: '#fff',
    padding: 16,
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1
  },
  outerRow: {
    flex: 1,
    padding: 8
  },
  name: {
    paddingBottom: 16,
    fontSize: 18,
    color: '#000'
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  avatar: {
    height: 80,
    width: 80,
    borderRadius: 40
  }
});

export default EventItem;
