import React from 'react';
import {StyleSheet, Image, View, Text, AsyncStorage, ActivityIndicator, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Octicons';
import TimeAgo from 'react-native-timeago';

const RepoItem = ({name, language, pushedAt, stargazersCount, openIssuesCount, id, forksCount, isPrivate, isFirst}) => {
  return (
    <View style={[styles.outerContainer, isFirst ? {marginTop: 96} : {}]}>
        <View style={styles.innerContainer}>
        <TouchableOpacity>
          <View style={styles.outerRow}>
            <Text style={styles.name}>{name}</Text>
            <Text>{isPrivate}</Text>
          </View>
          <View style={styles.outerRow}>
            <View  style={styles.row}>
              <Icon name="star"/>
              <Text>{stargazersCount || '0'}</Text>
            </View>
            <View  style={styles.row}>
              <Icon name="git-branch"/>
              <Text>{forksCount}</Text>
            </View>
            <View  style={styles.row}>
              <Icon name="issue-opened"/>
              <Text>{openIssuesCount}</Text>
            </View>
            <View  style={styles.row}>
              <Icon name="history"/>
              <Text><TimeAgo time={pushedAt}/></Text>
            </View>
            <View  style={styles.row}>
              <Text>{language}</Text>
            </View>
          </View>
          </TouchableOpacity>
        </View>
    </View>
  );
};

const styles = StyleSheet.create({
  outerContainer: {
    padding: 8,
    paddingLeft: 16,
    paddingRight: 16,
    elevation: 0
  },
  innerContainer: {
    borderRadius: 5,
    elevation: 2,
    backgroundColor: '#fff',
    padding: 16
  },
  outerRow: {
    flexDirection: 'row'
  },
  name: {
    paddingBottom: 16,
    fontSize: 18,
    color: '#000'
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingRight: 8
  }
});

export default RepoItem;
