import React from 'react';
import {StyleSheet, View, TextInput} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

const SearchBar = ({search, onTextChange}) => {
  return (
    <View style={styles.outerContainer}>
      <View style={styles.innerContainer}>
        <Icon name="search" size={24} />
        <TextInput style={styles.input} defaultValue="" placeholder="Search" editable={true} underlineColorAndroid="#fff" />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  outerContainer: {
    position: 'absolute',
    top: 0,
    padding: 8,
    paddingLeft: 16,
    paddingRight: 16,
    elevation: 0,
    width: '100%'
  },
  innerContainer: {
    borderRadius: 5,
    elevation: 2,
    backgroundColor: '#fff',
    padding: 8,
    flexDirection: 'row',
    alignItems: 'center'
  },
  input: {
    flex: 1,
    borderBottomWidth: 0
  },
  name: {
    paddingBottom: 16,
    fontSize: 18,
    color: '#000'
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingRight: 8
  }
});

export default SearchBar;
