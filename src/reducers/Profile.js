import {SET_PROFILE_DATA} from '../actions/types';

const INITIAL_STATE = {
  avatar_url: '',
  login: '',
  name: '',
};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case SET_PROFILE_DATA:
      return {
        ...state,
        avatar_url: action.payload.avatar_url,
        login: action.payload.login,
        name: action.payload.name,
        following: action.payload.following,
        followers: action.payload.followers,
        gists: action.payload.public_gists + action.payload.private_gists,
        repos: action.payload.public_repos + action.payload.total_private_repos
      };
    default:
      return state;
  }
}
