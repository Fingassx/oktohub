import {SET_REPOS} from '../actions/types';

const INITIAL_STATE = {};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case SET_REPOS:
      return {
        ...state,
        repos: Array.isArray(action.payload) && action.payload.map(item => {
          const {
            created_at: createdAt,
            default_branch: defaultBranch,
            description,
            forks_count: forksCount,
						full_name: fullName,
            id,
            private: isPrivate,
						language,
						name,
						open_issues_count: openIssuesCount,
						pushed_at: pushedAt,
						stargazersCount,
						watchers_count: watchersCount
          } = item;
          return {
            createdAt,
            defaultBranch,
            description,
            forksCount,
						fullName,
            id,
            isPrivate,
						language,
						name,
						openIssuesCount,
						pushedAt,
						stargazersCount,
						watchersCount
          };
        }) || null
      };
    default:
      return state;
  }
}
