import {persistCombineReducers} from 'redux-persist';
import {AsyncStorage} from 'react-native';
import profile from './Profile';
import repos from './Repos';
import events from './Events';

const config = {
  key: 'root',
  storage: AsyncStorage,
  blacklist: []
};

export default persistCombineReducers(config, {
  profile,
  repos,
  events
});
