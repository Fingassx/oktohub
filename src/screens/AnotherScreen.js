import React, {Component} from 'react';
import {StyleSheet, Image, View, Text} from 'react-native';

const BOTTOM_LABEL = 'By Signing up you agree to kenCharts';
const TERM_LABEL = 'Terms of Services & Privacy Policy';

class AnotherScreen extends Component {
  render() {
    return (
      <View style={{flex: 1}}>
        <Text
          style={styles.skip}
          onPress={() => {
            this.props.navigation.navigate('watchList');
          }}
        >
          Skip
        </Text>

        <View style={styles.bottomContainer}>
          <View style={[styles.innerContainer, styles.labelContainer]}>
            <Text style={styles.text}>{BOTTOM_LABEL}</Text>
            <Text style={[styles.text]}>{TERM_LABEL}</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  bottomContainer: {
    flex: 1,
    marginTop: 140
  },
  button: {
    margin: 7,
    borderWidth: 1,
    borderColor: 'white',
    height: 50
  },
  innerContainer: {
    flex: 1
  },
  labelContainer: {
    alignItems: 'center',
    justifyContent: 'flex-end',
    paddingBottom: 20
  },
  text: {
    color: 'white',
    lineHeight: 18,
    fontSize: 13,
    backgroundColor: 'transparent'
  },
  skip: {
    color: 'white',
    backgroundColor: 'transparent',
    position: 'absolute',
    right: 25,
    top: 38,
    fontSize: 15
  }
});

export default AnotherScreen;
