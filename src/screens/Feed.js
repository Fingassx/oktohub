import React, {Component} from 'react';
import {StyleSheet, Image, View, Text, AsyncStorage, FlatList} from 'react-native';
import {connect} from 'react-redux';
import {getUserEvents} from '../Api';
import {setUserEvents} from '../actions/EventsActions'
import EventIssueItem from '../components/EventItem'
import SearchBar from '../components/SearchBar'

class Feed extends Component {
  state = {
    events: []
  }
  componentWillMount() {
    this.getToken()
      .then(token => {
        this.setState({
          token
        })
        this.getLogin().then(login => {
          console.log(login)
          if (!login) {
            this.props.navigation.navigate('intro');
          }
          const events = getUserEvents(token, login);
          console.log(events)
          console.log(this.props)
          events.then(proj => this.props.setUserEvents(proj));
        })
      })
      .catch(err => {
        console.log(err)
        this.props.navigation.navigate('intro');
      });
  }
  componentWillReceiveProps (nextProps) {
    this.setState({
      events: nextProps.events.events
    })
  }
  getToken = () => {
    return new Promise((resolve, reject) => {
      AsyncStorage.getItem('token')
        .then(res => {
          if (res !== null) {
            resolve(res);
          } else {
            resolve(false);
          }
        })
        .catch(err => reject(err));
    });
  };
  getLogin = () => {
    return new Promise((resolve, reject) => {
      AsyncStorage.getItem('login')
        .then(res => {
          if (res !== null) {
            resolve(res);
          } else {
            resolve(false);
          }
        })
        .catch(err => reject(err));
    });
  };
  render() {
    const {events} = this.state;
    return (
      <View style={{flex: 1, backgroundColor: '#fff'}}>
        <View style={styles.bottomContainer}>
          <FlatList data={events.filter(event => event.type === 'IssueCommentEvent')} renderItem={({item, index}) => (
            <EventIssueItem {...item} isFirst={index === 0} />
          )} />
        </View>
        <SearchBar />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  bottomContainer: {
    flex: 1
  }
});

const mapStateToProps = state => ({
  events: state.events
})

export default connect(mapStateToProps, {setUserEvents})(Feed);
