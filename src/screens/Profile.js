import React, {Component} from 'react';
import {connect} from 'react-redux';
import {StyleSheet, Image, View, Text, AsyncStorage, ScrollView} from 'react-native';
import {getUserInfo} from '../Api';
import {setProfileData} from '../actions/ProfileActions';
import SearchBar from '../components/SearchBar';

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      avatar: props.profile.avatar_url,
      login: props.profile.login,
      following: 0,
      followers: 0,
      gists: 0,
      repos: 0
    };
  }
  componentDidMount() {
    this.getToken()
      .then((token) =>
        getUserInfo(token).then((info) => {
          this.props.setProfileData(info);
        })
      )
      .catch((err) => console.log(err));
  }
  componentWillReceiveProps(nextProps) {
    const {avatar_url, login, following, followers, gists, repos} = nextProps.profile;
    this.setState({
      avatar: avatar_url,
      login,
      following,
      followers,
      gists,
      repos
    });
  }
  getToken = () => {
    return new Promise((resolve, reject) => {
      AsyncStorage.getItem('token')
        .then((res) => {
          if (res !== null) {
            resolve(res);
          } else {
            resolve(false);
          }
        })
        .catch((err) => reject(err));
    });
  };
  render() {
    const {avatar, login, following, followers, gists, repos} = this.state;
    return (
      <View style={{flex: 1, backgroundColor: '#fff', position: 'relative'}}>
        <ScrollView>
          <View style={styles.bottomContainer}>
            <View style={styles.avatar}>
              <Image style={styles.avatar} source={{uri: avatar}} />
            </View>
            <Text style={styles.name}>{login}</Text>
          </View>
          <View style={styles.card}>
            <View style={styles.halfCard}>
              <Text>Followers</Text>
              <Text style={styles.numbers}>{followers}</Text>
            </View>
            <View style={styles.divider} />
            <View style={styles.halfCard}>
              <Text>Following</Text>
              <Text style={styles.numbers}>{following}</Text>
            </View>
          </View>
          <View style={styles.card}>
            <View style={styles.halfCard}>
              <Text>Repositories</Text>
              <Text style={styles.numbers}>{repos}</Text>
            </View>
            <View style={styles.divider} />
            <View style={styles.halfCard}>
              <Text>Gists</Text>
              <Text style={styles.numbers}>{gists}</Text>
            </View>
          </View>
        </ScrollView>
        <SearchBar />
      </View>
    );
  }
}

const mapStateToProps = (state) => ({
  profile: state.profile
});

const styles = StyleSheet.create({
  bottomContainer: {
    flex: 1,
    marginTop: 96,
    padding: 16,
    alignItems: 'center'
  },
  avatar: {
    height: 128,
    width: 128,
    borderRadius: 64,
    elevation: 4
  },
  name: {
    fontSize: 24,
    fontWeight: '800',
    color: '#000',
    paddingTop: 16
  },
  card: {
    borderRadius: 5,
    flexDirection: 'row',
    justifyContent: 'space-around',
    elevation: 2,
    padding: 16,
    margin: 16
  },
  halfCard: {
    width: '50%',
    alignItems: 'center'
  },
  divider: {
    height: '100%',
    width: 2,
    backgroundColor: '#ccc'
  },
  numbers: {
    fontSize: 28,
    color: '#000'
  }
});

export default connect(mapStateToProps, {setProfileData})(Profile);
