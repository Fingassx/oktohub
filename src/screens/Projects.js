import React, {Component} from 'react';
import {StyleSheet, Image, View, Text, AsyncStorage, ActivityIndicator, FlatList} from 'react-native';
import {connect} from 'react-redux';
import RepoItem from '../components/RepoItem';
import SearchBar from '../components/SearchBar';
import {setUserProjects} from '../actions/ReposActions';
import {getUserRepos} from '../Api';

class Projects extends Component {
  state = {
    isLoading: true,
    repos: []
  };
  componentWillMount() {
    this.getToken()
      .then((token) => {
        const projects = getUserRepos(token);
        projects.then((proj) => this.props.setUserProjects(proj));
      })
      .catch((err) => {
        console.log(err);
        this.props.navigation.navigate('intro');
      });
  }
  componentWillReceiveProps = (nextProps) => {
    console.log('PROPS>>>', nextProps);
    this.setState({
      isLoading: false,
      repos: nextProps.repos
    });
  };
  getToken = () => {
    return new Promise((resolve, reject) => {
      AsyncStorage.getItem('token')
        .then((res) => {
          if (res !== null) {
            resolve(res);
          } else {
            resolve(false);
          }
        })
        .catch((err) => reject(err));
    });
  };
  render() {
    console.log(this.props);
    return (
      <View style={styles.bottomContainer}>
        <View style={{flex: 1}}>
          {this.state.isLoading ? (
            <View style={{paddingTop: 96}}><ActivityIndicator size="large" color="#0000ff" /></View>
          ) : (
            <FlatList data={this.state.repos} renderItem={({item, index}) => <RepoItem key={item.id} isFirst={index===0} {...item} />} />
          )}
        </View>
        <SearchBar />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  bottomContainer: {
    flex: 1,
    backgroundColor: '#fff'
  }
});

const mapStateToProps = (state) => ({
  repos: state.repos.repos
});

export default connect(mapStateToProps, {setUserProjects})(Projects);
