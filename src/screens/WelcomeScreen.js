import React, {Component} from 'react';
import {StyleSheet, Image, View, Text, Button, TextInput, AsyncStorage} from 'react-native';
import {Base64} from 'js-base64';
import {connect} from 'react-redux';
import Icon from 'react-native-vector-icons/Octicons';
import {setProfileData} from '../actions/ProfileActions';
import {getUserInfo} from '../Api';

const setMyItem = async (token, key) => {
  try {
    await AsyncStorage.setItem(key, token);
  } catch (error) {
    console.error("can't save token");
  }
};

class WelcomeScreen extends Component {
  state = {
    login: '',
    password: '',
    error: ''
  };
  handlePress = () => {
    const token = Base64.encode(`${this.state.login}:${this.state.password}`);
    console.log(token);
    const geted = getUserInfo(token);
    if (geted) {
      console.log(geted);
      this.setState({
        error: ''
      });
      setMyItem(token, 'token');
      setMyItem(this.state.login, 'login');
      this.props.setProfileData(geted);
      this.props.navigation.navigate('Feed');
    } else {
      this.setState({
        error: 'Invalid login or password'
      });
    }
    console.log(geted);
  };
  render() {
    console.log(this.props);
    return (
      <View style={{flex: 1, backgroundColor: '#fff'}}>
        <View style={styles.bottomContainer}>
          <Icon name="octoface" size={200} color="#000" iconStyle={{width: '100%', height: '50%'}} />
          <TextInput placeholder="Login" style={styles.input} onChangeText={(login) => this.setState({login})} value={this.state.login} />
          <TextInput placeholder="Password" secureTextEntry={true} style={styles.input} onChangeText={(password) => this.setState({password})} value={this.state.password} />
          <View style={styles.input}><Button title="Log In" style={styles.input} onPress={this.handlePress} /></View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => ({
  profile: state.profile
});

const styles = StyleSheet.create({
  bottomContainer: {
    flex: 1,
    padding: 16,
    justifyContent: 'center',
    alignItems: 'center'
  },
  input: {
    width: '100%',
    height: 40,
    marginTop: 16
  }
});

export default connect(mapStateToProps, {setProfileData})(WelcomeScreen);
